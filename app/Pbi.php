<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pbi extends Model
{
    protected $fillable = [
        'titulo',
        'descripcion',
    ];

    public function project(){
		    return $this->belongsTo('App\Project');
    }

    public function tasks()
    {
    	  return $this->hasMany('App\Task');
    }
}
