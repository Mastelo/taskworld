<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Pbi;

class PbisController extends Controller
{
    /**
      * Display the specified resource.
      *
      * @param  \App\Pbi  $project
      * @return \Illuminate\Http\Response
      */
    public function show(Pbi $pbi)
    {
        $idpbi = Pbi::find($pbi->id);
 
        //$comments = $project->comments;
        $pbis =  \DB::table('pbis')
        ->where('pbis.id', $idpbi->id)
        ->get();
        //dd($pbis);
        
         return view('tasks.show', [  'pbis'=>$pbis ]);
    }
}
