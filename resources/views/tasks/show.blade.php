@extends('layouts.app')

@section('content')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-comment"></span> 
                PBI's

                <ul class="list-group">
                    @foreach($pbis as $pbi)
                        <li class="list-group-item"> 
                            <i class="fa fa-play" aria-hidden="true"></i>
                            <a href="/tasks/{{ $pbi->id }}" > {{ $pbi->titulo }}</a>
                            <a> {{ $pbi->descripcion }} </a>
                        </li>
                    @endforeach
                </ul>
             </h3>
        </div> 
    </div>
@endsection